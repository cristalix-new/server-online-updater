FROM node:latest AS builder
WORKDIR /app/
COPY yarn.lock package.json ./
RUN yarn install --pure-lockfile
COPY ./ ./
RUN yarn build

FROM node:latest
WORKDIR /app/
COPY yarn.lock package.json ./
RUN yarn install --production=true --pure-lockfile
COPY .env /app/.env
COPY --from=builder /app/dist/ /app/dist/
VOLUME /app/files /app/clients
EXPOSE 8080
CMD yarn start