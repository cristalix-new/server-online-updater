local is_production = std.extVar('IS_PRODUCTION') == 'true';
local namespace = std.extVar('KUBE_NAMESPACE');
local base_domain = std.extVar('BASE_DOMAIN');

[
  {
    kind: 'Secret',
    apiVersion: 'v1',
    metadata: {
      name: 'pull-credentials',
    },
    data: {
      '.dockerconfigjson': std.base64(std.manifestJson({
        auths: {
          [std.extVar('CI_REGISTRY')]: {
            auth: std.base64(std.extVar('CI_DEPLOY_USER') + ':' + std.extVar('CI_DEPLOY_PASSWORD'))
          },
        },
      })),
    },
    type: 'kubernetes.io/dockerconfigjson',
  },

  {
    apiVersion: 'batch/v1beta1',
    kind: 'CronJob',
    metadata: {
      name: 'server-online-updater',
    },
    spec: {
      schedule: "*/1 * * * *",
      jobTemplate: {
        spec: { 
          template: {
            spec: {
              containers: [
                {
                  image: '%s:%s' % [std.extVar('CI_REGISTRY_IMAGE'), std.extVar('CI_COMMIT_SHA')],
                  name: 'server-online-updater',
                  env: [
                    {
                      name: 'REDIS_URL',
                      value: 'redis://redis.common-services:6379/'
                    },
                    {
                      name: 'DATABASE_HOST',
                      value: 'postgres.common-services'
                    },
                    {
                      name: 'TOWER_URL',
                      value: 'http://tower-rest-service.common-services'
                    },
                  ],
                  volumeMounts: [
                    {
                      name: 'config',
                      mountPath: '/app/config',
                      readOnly: true
                    }
                  ]
                },
              ],
              volumes: [
                {
                  name: 'config',
                  configMap: {
                    name: 'config'
                  }
                }
              ],
              imagePullSecrets: [
                {
                  name: 'pull-credentials'
                },
              ],
              restartPolicy: 'Never'
            },
          },
        },
      },
    },
  },
]