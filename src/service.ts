import Bluebird from "bluebird";
// eslint-disable-next-line @typescript-eslint/no-explicit-any
global.Promise = <any>Bluebird;

import {Pool, QueryConfig, QueryResult, QueryResultRow} from "pg";
import dotenv from "dotenv";
import {promises as fsPromise} from "fs";
import {Record, Array, String, Number} from "runtypes";
import {createNodeRedisClient} from "handy-redis";
import {getStatus} from "mc-server-status";
import TowerConnecter from './TowerConnecter';

dotenv.config();

const ConfigType = Array(Record({
    redisServerId: String,
    hosts: Array(Record({
        hostname: String,
        port: Number
    }))
}));

type RecordType = {
    record: number,
}

(async () => {
    const config = ConfigType.check(JSON.parse((await fsPromise.readFile("config/servers.json")).toString()));
    const redis = createNodeRedisClient({
        url: process.env.REDIS_URL ?? "redis://127.0.0.1:6379"
    });
    const poolConfig = {
        host: process.env.DATABASE_HOST || "127.0.0.1",
        port: parseInt(<string>process.env.DATABASE_PORT) || 5432,
        user: process.env.DATABASE_USER || "database-user",
        password: process.env.DATABASE_PASSWORD || "database-password",
        database: process.env.DATABASE_DATABASE || "database-database"
    };
    const pool = new Pool({...poolConfig});
    const tower = new TowerConnecter(process.env.TOWER_URL || 'http://127.0.0.1');
    let onlineRecord = (await pool.query<RecordType>("SELECT record FROM online_record;")).rows[0].record;
    let currentOnlineAll = 0;
    await Promise.all(config.map(async (server) => {
        console.error(`updating ${server.redisServerId}...`);

        const statuses = await Promise.all(server.hosts.map(async (host) => {
            try {
                return await getStatus(host.hostname, host.port, {
                    checkPing: false
                });
            } catch (e) {
                console.error(`error pinging ${server.redisServerId}/${host.hostname}:${host.port}: ${e}`);
                return null;
            }
        }));

        const succeed = statuses.filter(status => status);

        const isOnline = succeed.length > 0;
        const currentPlayers = succeed.map(status => status?.players?.online ?? 0)
            .reduce((p, c) => p + c, 0);
        const maxPlayers = succeed.map(status => status?.players?.max ?? 0)
            .reduce((p, c) => p + c, 0);

        currentOnlineAll += currentPlayers;
        await Promise.all([redis.set(`server_${server.redisServerId}_is_online`, isOnline ? "1" : "0"),
            redis.set(`server_${server.redisServerId}_current_players`, currentPlayers.toString()),
            redis.set(`server_${server.redisServerId}_max_players`, maxPlayers.toString())]);
        console.error(`${server.redisServerId} updated successfully`);
    }));
    if (currentOnlineAll > onlineRecord) {
      onlineRecord = currentOnlineAll;
      await pool.query("UPDATE online_record SET record = $1 WHERE true", [currentOnlineAll]);
    }
    await redis.set('online_record', "" + onlineRecord);
    const towerData = await tower.createRating();
    await redis.set('rating_entries', towerData.map(e => `${e.id}:${e.online}`).join('@'));
    process.exit(0);
})().catch(e => {
    console.error(e);
    process.exit(1);
});