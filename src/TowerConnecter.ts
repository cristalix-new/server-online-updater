import axios from 'axios';

type TowerResponse = {
    data: PackageData,
}

type PackageData = {
    data: Package,
}

type Package = {
    realms: Realm[],
}

type Realm = {
    groupName?: string,
    realmKind?: string,
    realmId: RealmId,
    servicedServers?: string[],
    currentPlayers: number,
}

type RealmId = {
    typeName: string,
    id: number,
}

export type RatingEntry = {
    id: string,
    online: number,
}

export default class TowerConnecter {

    constructor(public url: string) { }

    async createRating(): Promise<RatingEntry[]> {
        const data = (await this.getData()).data.data.realms;
        const realms: { [key: string]: any } = {};
        data.forEach(realm => {
            if ((realm.realmKind && realm.realmKind === 'SERVICE') || realm.realmId.id < 0 || realm.realmId.typeName === 'TEST')
                return;
            const realmType = realm.realmId.typeName;
            let realmData = realms[realmType];
            if (!realmData) {
                realmData = {
                    current: 0,
                    servicedServers: {}
                };
                if (realm.groupName) {
                    realmData.groupName = realm.groupName;
                }
                realms[realmType] = realmData;
            }
            if (realm.groupName) {
                realmData.groupName = realm.groupName;
            }
            realmData.current += realm.currentPlayers;
            if (realm.servicedServers) {
                realm.servicedServers.forEach(serviced => {
                    if (serviced === realmType)
                        return;
                    realmData.servicedServers[serviced] = true;
                });
            }
        });
        let working = true;
        const alreadyDeleted: string[] = [];
        const requests: {[key: string]: string[]} = {};

        while (working) {
            working = false;
            const toDelete: string[] = [];
            [...Object.entries(realms)].forEach(entry => {
                const toDeleteServiced: string[] = [];
                [...Object.keys(entry[1].servicedServers)].forEach(subRealm => {
                    if (!requests[subRealm]) requests[subRealm] = [];
                    requests[subRealm].push(entry[0]);
                    if (alreadyDeleted.includes(subRealm) || toDelete.includes(subRealm)) {
                        toDeleteServiced.push(subRealm);
                        return;
                    }
                    if (!realms[subRealm]) {
                        toDeleteServiced.push(subRealm);
                        return;
                    }
                    const realmData = realms[subRealm];
                    if ([...Object.keys(realmData.servicedServers)].length === 0) {
                        entry[1].current += realmData.current;
                        toDeleteServiced.push(subRealm);
                        toDelete.push(subRealm);
                        working = true;
                    }
                });
                toDeleteServiced.forEach(subRealm => {
                    delete entry[1].servicedServers[subRealm];
                });
            });
            toDelete.forEach(subRealm => {
                delete realms[subRealm];
                alreadyDeleted.push(subRealm);
            });
        }

        const results: { [key: string]: RatingEntry } = {};

        [...Object.entries(realms)].forEach(entry => {
            if (!entry[1].groupName)
                return;
            let rating = results[entry[1].groupName];
            if (!rating) {
                rating = {
                    id: entry[1].groupName,
                    online: 0,
                }
                results[entry[1].groupName] = rating;
            }
            rating.online += entry[1].current;
        });

        return [...Object.values(results)].sort((a, b) => b.online - a.online);
    }

    private async getData(): Promise<TowerResponse> {
        const body = `{"className":"ru.cristalix.core.network.packages.AllRealmsPackage","data":{}}`;
        const data = (await axios.post(`${this.url}/api/request`, body, {
            headers: {
                'Content-Type': 'application/json',
            }
        })).data;
        return data as TowerResponse;
    }

}